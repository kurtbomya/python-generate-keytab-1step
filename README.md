# python-generate-keytab-1step

### Purpose
- Enable transparent handshake between Ansible, Kerberos, Keytab generation, and ultimately to a Windows machine.
- Remove the need to prepare keytab files ahead of time.
- Remove the need to update keytab files during password rotations.
- Utilize the Ansible shell module to execute the included command.
- Couple the username and password call into a single step.

### Requires
pexpect & docopt
```
pip install pexpect
pip install docopt
```

### Usage

Include the keytab.py file in the deployment.
```yml
- hosts: localhost
  gather_facts: False
  tasks:
     - name: Create Kerberos keytab file
       shell: python keytab.py -u {{ username }} --domain=BACON.CORP.COM --keytab=test.keytab --password {{ password }}
       delegate_to: 127.0.0.1
```

All together now,
1. Kdestroy
1. Create Keytab File with this package through ktutil
1. Kinit
1. Do stuff to target Windows machine
1. Kdestroy

```yml
---
- hosts: localhost
  gather_facts: False
  tasks:
     - name: Destroy Kerberos
       shell: kdestroy
       delegate_to: 127.0.0.1
- hosts: localhost
  gather_facts: False
  tasks:
     - name: Create Kerberos keytab file
       shell: python keytab.py -u {{ username }} --domain=BACON.CORP.COM --keytab={{ username }}.keytab --password {{ password }}
       delegate_to: 127.0.0.1
- hosts: localhost
  gather_facts: False
  tasks:
     - name: Initialize Kerberos
       shell: kinit -t {{ username }}.keytab {{ username }}@BACON.CORP.COM
       delegate_to: 127.0.0.1
- hosts: <target-hosts>
  roles:
    - "<target-hosts>"
- hosts: localhost
  gather_facts: False
  tasks:
     - name: Destroy Kerberos
       shell: kdestroy
       delegate_to: 127.0.0.1
```

### Credit
Forked from Tagar/stuff
